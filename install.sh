#!/usr/bin/env bash

set -euo pipefail

[[ -z "${1+x}" ]] && >&2 echo "url is required as first argument" && exit 1
[[ -z "${DAPPL_OS_TYPE+x}" ]] && >&2 echo "DAPPL_OS_TYPE is required" && exit 1
[[ "${DAPPL_OS_TYPE}" != "linux" ]] && >&2 echo "OS type is not linux" && exit 1
[[ -z "${DAPPL_OS_DISTRO+x}" ]] && >&2 echo "DAPPL_OS_DISTRO is required" && exit 1
[[ $EUID -ne 0 ]] && >&2 echo "must be run as root user" && exit 1

URL="${1}"

function install_apt() {
  export DEBIAN_FRONTEND="noninteractive"
  apt-get update -qq && apt-get install -qq curl
  echo "Downloading ${URL}"
  curl -fksSL "${URL}" -o /tmp/file.deb
  dpkg -i /tmp/file.deb
  rm -f /tmp/file.deb
}

function install_yum() {
  yum install -y -q curl
  echo "Downloading ${URL}"
  curl -fksSL "${URL}" -o /tmp/file.rpm
  rpm -i /tmp/file.rpm
  rm -f /tmp/file.rpm
}

case "${DAPPL_OS_DISTRO}" in
  debian) install_apt ;;
  ubuntu) install_apt ;;
  centos) install_yum ;;
  ol) install_yum ;;
  *) >&2 echo "Unsupported distribution ${DAPPL_OS_DISTRO}" && exit 1
esac
